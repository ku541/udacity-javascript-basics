var bio = {
  "name":"Kusal Wettimuny",
  "role":"Developer",
  "contacts":{
    "mobile":"076-767-0666",
    "email":"kusalwettimuny@outlook.com",
    "github":"LukasMutiny",
    "googlePlus":"Kusal Wettimuny",
    "location":"Colombo, Sri Lanka"
  },
  "welcomeMessage":"Hello There. :)",
  "skills":["Perfectionist", "Purist"],
  "biopic":"images/bioPic.jpg"
}

var education = {
  "schools":[
    {
      "name":"SLIIT",
      "location":"Malabe, Sri Lanka",
      "degree":"BSc",
      "major":"IT",
      "dates":"2014 - 2017",
      "url":"http://www.sliit.lk/"
    },
    {
      "name":"D. S. Senanayake College",
      "location":"Colombo, Sri Lanka",
      "degree":"O/L, A/L",
      "major":"Mathematics",
      "dates":"1999 - 2012"
    }
  ],
  "onlineCourses":[
    {
      "title":"Android Development for Beginners",
      "school":"Udacity",
      "dates":"December 2015",
      "url":"https://www.udacity.com/course/android-development-for-beginners--ud837"
    },
    {
      "title":"Intro to HTML and CSS",
      "school":"Udacity",
      "dates":"January 2016",
      "url":"https://www.udacity.com/courses/ud304"
    },
    {
      "title":"Javascript Basics",
      "school":"Udacity",
      "dates":"February 2016",
      "url":"https://www.udacity.com/courses/ud804"
    }
  ]
}

var work = {
  "jobs":[
    {
      "employer":"Self-Employed",
      "title":"Developer",
      "location":"Colombo, Sri Lanka",
      "dates":"February 2017 - February 2019",
      "description":"Web Application Development, Native Application Development"
    },
    {
      "employer":"Google",
      "title":"UX Engineer",
      "location":"Mountain View, California, USA",
      "dates":"February 2019 Onwards",
      "description":"Deliver innovative, engaging prototypes using the latest in front-end technologies. Advise, collaborate with, and synthesize feedback from UX designers and researchers. Fulfill several project requests simultaneously while meeting tight deadlines. Build and maintain innovative and engaging websites that power projects at Google[x]."
    }
  ]
}

var projects = {
  "projects":[
    {
      "title":"Project Aura",
      "dates":"January 2016",
      "description":"Your job as UX Designer for Project Aura is to define all aspects of the experience of our products. You'll address complex technologies and transform them into intuitive, accessible and easy-to-use interactions for people around the world – from the first-time user to the sophisticated expert. Achieving this goal requires collaboration with teams of Industrial Designers, Researchers, Engineers, Product Managers, and your own fellow UX team throughout the design process – from creating interaction models and prototypes, to designing complete user interfaces ready for production. At each stage, you will anticipate what our users need, advocate for them and ensure that the final product surprises, delights and absolutely works.",
      "images": ["https://9to5google.files.wordpress.com/2016/01/untitled1.png"]
    }
  ]
}

function displayBio() {
  var formattedHeaderName = HTMLheaderName.replace("%data%", bio.name);
  var formattedHeaderRole = HTMLheaderRole.replace("%data%", bio.role);
  $("#header").prepend(formattedHeaderName, formattedHeaderRole);

  var formattedMobile = HTMLmobile.replace("%data%", bio.contacts.mobile);
  var formattedEmail = HTMLemail.replace("%data%", bio.contacts.email);
  var formattedGooglePlus = HTMLgooglePlus.replace("%data%", bio.contacts.googlePlus);
  var formattedGitHub = HTMLgithub.replace("%data%", bio.contacts.github);
  var formattedLocation = HTMLlocation.replace("%data%", bio.contacts.location);
  $("#topContacts").append(formattedMobile);
  $("#topContacts").append(formattedEmail);
  $("#topContacts").append(formattedGooglePlus);
  $("#topContacts").append(formattedGitHub);
  $("#topContacts").append(formattedLocation);

  var formattedBioPic = HTMLbioPic.replace("%data%", bio.biopic);
  var formattedWelcomeMsg = HTMLwelcomeMsg.replace("%data%", bio.welcomeMessage);
  $("#header").append(formattedBioPic);
  $("#header").append(formattedWelcomeMsg);

  $("#header").append(HTMLskillsStart);
  for (var skill in bio.skills) {
    var formattedSkill = HTMLskills.replace("%data%", bio.skills[skill]);
    $("#header").append(formattedSkill);
  }
}

function displayEducation() {
  for (school in education.schools) {
    var formattedSchoolName = HTMLschoolName.replace("%data%", education.schools[school].name);
    var formattedSchoolLocation = HTMLschoolLocation.replace("%data%", education.schools[school].location);
    var formattedSchoolDates = HTMLschoolDates.replace("%data%", education.schools[school].dates);
    var formattedSchoolMajor = HTMLschoolMajor.replace("%data%", education.schools[school].major);
    var formattedSchoolDegree = HTMLschoolDegree.replace("%data%", education.schools[school].degree);
    $("#education").append(HTMLschoolStart);
    $(".education-entry:last").append(formattedSchoolName + formattedSchoolDegree);
    $(".education-entry:last").append(formattedSchoolLocation);
    $(".education-entry:last").append(formattedSchoolDates);
    $(".education-entry:last").append(formattedSchoolMajor);
  }
}

function displayOnlineCourses() {
  for (course in education.onlineCourses) {
    var formattedOnlineTitle = HTMLonlineTitle.replace("%data%", education.onlineCourses[course].title);
    var formattedOnlineSchool = HTMLonlineSchool.replace("%data%", education.onlineCourses[course].school);
    var formattedOnlineDates = HTMLonlineDates.replace("%data%", education.onlineCourses[course].dates);
    var formattedOnlineURL = HTMLonlineURL.replace("%data%", education.onlineCourses[course].url);
    $("#courses").append(HTMLonlineStart);
    $(".course-entry:last").append(formattedOnlineTitle+ formattedOnlineSchool);
    $(".course-entry:last").append(formattedOnlineDates);
    $(".course-entry:last").append(formattedOnlineURL);
  }
}

function displayWork() {
  for (var job in work.jobs) {
    var formattedEmployer = HTMLworkEmployer.replace("%data%", work.jobs[job].employer);
    var formattedTitle = HTMLworkTitle.replace("%data%", work.jobs[job].title);
    var formattedLocation = HTMLworkLocation.replace("%data%", work.jobs[job].location);
    var formattedDates = HTMLworkDates.replace("%data%", work.jobs[job].dates);
    var formattedDescription = HTMLworkDescription.replace("%data%", work.jobs[job].description);
    $("#workExperience").append(HTMLworkStart);
    $(".work-entry:last").append(formattedEmployer + formattedTitle);
    $(".work-entry:last").append(formattedLocation);
    $(".work-entry:last").append(formattedDates);
    $(".work-entry:last").append(formattedDescription);
  }
}

function displayProject() {
  for (project in projects.projects) {
    var formattedProjectTitle = HTMLprojectTitle.replace("%data%", projects.projects[project].title);
    var formattedProjectDates = HTMLprojectDates.replace("%data%", projects.projects[project].dates);
    var formattedProjectDescription = HTMLprojectDescription.replace("%data%", projects.projects[project].description);
    $("#projects").append(HTMLprojectStart);
    $(".project-entry:last").append(formattedProjectTitle);
    $(".project-entry:last").append(formattedProjectDates);
    $(".project-entry:last").append(formattedProjectDescription);
    for (image in projects.projects[project].images)  {
      var formattedProjectImages = HTMLprojectImage.replace("%data%", projects.projects[project].images);
      $(".project-entry:last").append(formattedProjectImages);
    }
  }
}

function displayMap() {
  $("#mapDiv").append(googleMap);
}

function displayFooter() {
  var formattedMobile = HTMLmobile.replace("%data%", bio.contacts.mobile);
  var formattedEmail = HTMLemail.replace("%data%", bio.contacts.email);
  var formattedGooglePlus = HTMLgooglePlus.replace("%data%", bio.contacts.googlePlus);
  var formattedGitHub = HTMLgithub.replace("%data%", bio.contacts.github);
  var formattedLocation = HTMLlocation.replace("%data%", bio.contacts.location);
  $("#footerContacts").append(formattedMobile);
  $("#footerContacts").append(formattedEmail);
  $("#footerContacts").append(formattedGooglePlus);
  $("#footerContacts").append(formattedGitHub);
  $("#footerContacts").append(formattedLocation);
}

displayBio();
displayEducation();
displayOnlineCourses();
displayWork();
displayProject();
displayMap();
displayFooter();
