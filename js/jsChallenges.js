function getRelationship(x, y) {

  if (isNaN(x) == true && isNaN(y) == true) {
    var str5 = "Can't compare relationships because ";
    var str6 = " and ";
    var str7 = " are not numbers.";
    console.log(str5.concat(x).concat(str6).concat(y).concat(str7));
  }

  else if (isNaN(x) == true) {
    var str1 = "Can't compare relationships because ";
    var str2 = " is not a number.";
    console.log(str1.concat(x).concat(str2));
  }

  else if (isNaN(y) == true) {
    var str3 = "Can't compare relationships because ";
    var str4 = " is not a number.";
    console.log(str1.concat(y).concat(str2));
  }

  else if (x < y) {
    console.log("<");
  }

  else if (x > y) {
    console.log(">");
  }

  else if (x == y) {
    console.log("=");
  }
}

var list = [
  "Neil Armstrong",
  "Buzz Aldrin",
  "Pete Conrad",
  "Alan Bean",
  "Alan Shepard",
  "Edgar Mitchell",
  "David Scott",
  "James Irwin",
  "John Young",
  "Charles Duke",
  "Eugene Cernan",
  "Harrison Schmitt"
];
function alphabetizer(names) {
  var newName = [];
  for (var i = 0; i < names.length; i++) {
    newName = names[i].split(" ");
    newName = newName[1].concat(", ").concat(newName[0]);
    console.log(newName);
  }
}

psiResults = {
  "kind": "pagespeedonline#result",
  "id": "/speed/pagespeed",
  "responseCode": 200,
  "title": "PageSpeed Home",
  "score": 90,
  "pageStats": {
    "numberResources": 22,
    "numberHosts": 7,
    "totalRequestBytes": "2761",
    "numberStaticResources": 16,
    "htmlResponseBytes": "91981",
    "cssResponseBytes": "37728",
    "imageResponseBytes": "13909",
    "javascriptResponseBytes": "247214",
    "otherResponseBytes": "8804",
    "numberJsResources": 6,
    "numberCssResources": 2
  },
  "formattedResults": {
    "locale": "en_US",
    "ruleResults": {
      "AvoidBadRequests": {
        "localizedRuleName": "Avoid bad requests",
        "ruleImpact": 0.0
      },
      "MinifyJavaScript": {
        "localizedRuleName": "Minify JavaScript",
        "ruleImpact": 0.1417,
        "urlBlocks": [{
          "header": {
            "format": "Minifying the following JavaScript resources could reduce their size by $1 ($2% reduction).",
            "args": [{
              "type": "BYTES",
              "value": "1.3KiB"
            }, {
              "type": "INT_LITERAL",
              "value": "0"
            }]
          },
          "urls": [{
            "result": {
              "format": "Minifying $1 could save $2 ($3% reduction).",
              "args": [{
                "type": "URL",
                "value": "http://code.google.com/js/codesite_tail.pack.04102009.js"
              }, {
                "type": "BYTES",
                "value": "717B"
              }, {
                "type": "INT_LITERAL",
                "value": "1"
              }]
            }
          }, {
            "result": {
              "format": "Minifying $1 could save $2 ($3% reduction).",
              "args": [{
                "type": "URL",
                "value": "http://www.gmodules.com/ig/proxy?url\u003dhttp%3A%2F%2Fjqueryjs.googlecode.com%2Ffiles%2Fjquery-1.2.6.min.js"
              }, {
                "type": "BYTES",
                "value": "258B"
              }, {
                "type": "INT_LITERAL",
                "value": "0"
              }]
            }
          }]
        }]
      },
      "SpriteImages": {
        "localizedRuleName": "Combine images into CSS sprites",
        "ruleImpact": 0.0
      }
    }
  },
  "version": {
    "major": 1,
    "minor": 11
  }
}
function totalBytes(psiResults) {
  var total = 0;
  for (var attr in psiResults.pageStats) {
    if (attr.match(/bytes/gi)) {
      total += Number(psiResults.pageStats[attr]);
    }
  }
  console.log(total);
}

function ruleList(psiResults) {
  var ruleNames = [];
  for (var name in psiResults.formattedResults.ruleResults) {
    ruleNames.push(psiResults.formattedResults.ruleResults[name].localizedRuleName);
  }
  console.log(ruleNames);
}
